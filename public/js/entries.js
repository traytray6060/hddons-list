// Handles entries

const CATEGORY_NAMES = {
	about: "HDest Addons List",
	followers: "Followers",
	maps: "Maps",
	misc: "Miscellaneous",
	monsters: "Monsters",
	mugshots: "Mugshots",
	mutators: "Mutators",
	overhauls: "Overhauls",
	skins: "Skins",
	uas: "Ugly as Sin",
	utilities: "Utilities",
	weapons: "Weapons"
};

const BASE_LINK = "https://gitlab.com/api/v4/projects/31095835/repository/files";
const END_LINK = "/raw?ref=master";

// The id of the addons section
const ENTRIES_ID = "entries";
const PINNED_ID = "pinned";

// This is very important
var isBuilding = false;

// Removes all addon entries.
function clearEntries() {
	console.log("Clearing entries...")
	document.getElementById(ENTRIES_ID).remove();
	document.getElementById("about").innerHTML = "";
	document.getElementById("date").innerHTML = "Last Updated: DD/MM/YYYY";

	let entries = document.createElement("section");
	let pinned = document.createElement("section");

	entries.id = ENTRIES_ID;
	pinned.id = PINNED_ID;
	entries.appendChild(pinned);

	let addons = document.getElementById("addons");
	addons.insertAdjacentElement("beforeend", entries);
}

// Builds all the addon entries. prefix: The subdirectory
function buildEntries(prefix, targetEntry) {
	if (isBuilding) {
		return;
	}
	isBuilding = true;

	// Disable all category buttons
	disableCategories();
	clearEntries();
	clearTags();

	let xhr = new XMLHttpRequest();
	let page = BASE_LINK + "/" + prefix + "_list.json" + END_LINK;

	// Change title
	document.title = "hddons-list - " + CATEGORY_NAMES[prefix];

	xhr.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			let progress = document.getElementById("progress");
			progress.innerHTML = "Building entries...";

			// Only show entries when done building
			let entries = document.getElementById(ENTRIES_ID);
			entries.style.visibility = "hidden";

			let file = JSON.parse(this.responseText);

			// About
			let about = document.getElementById("about")
			about.innerHTML = file.about;

			// Last updated
			let date = document.getElementById("date");
			if (prefix === "about")
				date.innerHTML = "Last Updated: none (go check the categories!)";
			else
				date.innerHTML = "Last Updated: " + file.last_updated;

			let target = document.getElementById("title");

			for (let entry in file.list) {
				let e = file.list[entry];

				addEntry(e, prefix);
			}
			isBuilding = false;
			enableCategories();

			// Hide loading text and show entries
			let loading = document.getElementById("loading");
			loading.style.height = 0;
			loading.style.visibility = "hidden";

			entries.style.visibility = "visible";

			if (targetEntry != null) {
				// Go to entry
				let tE = document.getElementById(targetEntry)
				tE.scrollIntoView({
					behaviour: "auto",
					block: "center",
					inline: "center"
				});
				tE.classList.add("highlight");
			}

			createTagButtons();
		}
	}

	xhr.open("GET", page);
	xhr.send();

	// Show loading text
	let loading = document.getElementById("loading");
	loading.style.height = "100%";
	loading.style.visibility = "visible";
	loading.children[1].innerHTML = "Fetching " + prefix + "_list.json";
}

function addEntry(e, currentCategory) {
	// Get variables
	let title = e.title;
	let credits = e.credits;
	let flairs = e.flairs;
	let desc = e.description;
	let flag = e.flag;
	let tags = e.tags;

	console.log("Adding entry: " + title);
	section_id = ENTRIES_ID;

	// create a new addon
	let entry = document.createElement("section");
	entry.classList.add("entry");

	// Check for flags
	if (flag) {
		entry.classList.add(flag);

		if (flag == PINNED_ID) {
			section_id = PINNED_ID;
		}
	}

	// Set id
	let id = title.replace(/\W/g, "").toLowerCase() + "-" + credits.replace(/^by /g, "").replace(/\W/g, "").toLowerCase();
	entry.id = id;

	// Create the essentials
	let h = document.createElement("div"); // Header
	let t = document.createElement("h3"); // Title
	let c = document.createElement("h5"); // Credits
	let f = createFlairs(flairs); //Flairs
	let d = document.createElement("div"); // Description
	let s = document.createElement("p"); // Tags

	// Set values
	t.innerHTML = title;
	c.innerHTML = credits;
	d.innerHTML = desc;

	// Add tags (which are just placeholder classes)
	if (tags) {
		addTags(entry, tags);
		let txt = "tags: ";
		for (let i = 0; i < tags.length; i++) {
			txt = txt + tags[i];
			if (i + 1 != tags.length) {
				txt = txt + ", ";
			}
		}
		s.innerHTML = txt;
		s.classList.add("tags");
	}

	// Append stuff to entry
	console.log("Appending elements to addon entry...");
	h.appendChild(t);
	h.appendChild(c);
	entry.appendChild(h);
	entry.appendChild(document.createElement("hr"));
	entry.appendChild(f);
	entry.appendChild(d);
	entry.appendChild(s);

	// Add stuff for devs
	// you don't need to copy the "about" page link
	if (currentCategory != "about") {
		let entryLink = document.createElement("a");
		let entryId = document.createElement("a");

		// Icons
		let linkIcon = document.createElement("img");
		let hashIcon = document.createElement("img");
		linkIcon.src = "icons/link.svg";
		hashIcon.src = "icons/hash.svg";

		// Tooltip
		let tLink = document.createElement("span");
		let tId = document.createElement("span");

		entryLink.onmouseover = event =>
			tLink.innerHTML = "Copy link to entry";
		entryId.onmouseover = event =>
			tId.innerHTML = "Copy entry id";

		entryLink.addEventListener("click", function() {
			// just in case
			let link = window.location.href.split("#")[0] + "#" + currentCategory + ":" + id;
			navigator.clipboard.writeText(link);
			tLink.innerHTML = "Copied!"
		});
		entryId.addEventListener("click", function() {
			navigator.clipboard.writeText("#" + currentCategory + ":" + id);
			tId.innerHTML = "Copied!"
		});

		// Append it all
		entryLink.appendChild(tLink);
		entryId.appendChild(tId);

		entryLink.appendChild(linkIcon);
		entryId.appendChild(hashIcon);

		t.appendChild(entryLink);
		t.appendChild(entryId);
	}

	// Add the entry
	let target = document.getElementById(section_id);
	target.insertAdjacentElement("beforeend", entry);
}

function createFlairs(flairs) {
	console.log("Creating flairs...");
	let fs = document.createElement("flairs");
	for (let f in flairs) {
		let div = document.createElement("div");
		console.log("Appending flair \"" + flairs[f] + "\" with text: \"" + f + "\"...");

		div.innerHTML = f;
		div.classList.add(flairs[f]);

		fs.appendChild(div);
	}

	return fs;
}
