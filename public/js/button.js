var categories = [];

document.addEventListener("DOMContentLoaded", function(event) {
	categories = document.getElementById("categories").children;

	for (i = 0; i < categories.length; i++) {
		categories[i].addEventListener("click", function() {
			selectButton(this);
			window.location.href = this.id;
		});
	}
});

function selectButton(button) {
	for (i = 0; i < categories.length; i++) {
		categories[i].classList.remove("selected");
	}

	button.classList.add("selected");
}

function disableCategories() {
	for (i = 0; i < categories.length; i++) {
		categories[i].disabled = true;
	}
}

function enableCategories() {
	for (i = 0; i < categories.length; i++) {
		categories[i].disabled = false;
	}
}
