// Shitty tag system that I thought of
// Maybe when I get to working on a server of sorts, I'll properly implement tags :]

const TAG_PREFIX = "t_";

var addedTags = [];
var tagFilter = "";

function stringToTag(string) {
	return TAG_PREFIX + string.replace(/\W/g, "-").toLowerCase();
}

function addTags(element, tags) {
	for (let i = 0; i < tags.length; i++) {
		element.classList.add(stringToTag(tags[i]));
		if (addedTags.find(tag => tag === tags[i]) == undefined) {
			addedTags.push(tags[i]);
		}
	}
	addedTags.sort();
}

function clearTags() {
	document.getElementById("subcategories").remove();
	let header = document.getElementsByClassName("header");
	let div = document.createElement("div");
	div.id = "subcategories";
	header[0].insertAdjacentElement("afterbegin", div);

	addedTags = [];
	tagFilter = "";
}

function createTagButtons() {
	let div = document.getElementById("subcategories");
	if (addedTags.length == 0) {
		return;
	}

	div.innerHTML = "Filter by tag: ";
	for (let i = 0; i < addedTags.length; i++) {
		let button = document.createElement("button");
		button.innerHTML = addedTags[i];
		button.addEventListener("click", function() {
			if (this.classList.contains("selected")) {
				this.classList.remove("selected");
			} else {
				this.classList.add("selected");
			}

			// Update filter
			let filters = document.getElementById("subcategories").getElementsByClassName("selected");
			tagFilter = "";
			for (let i = 0; i < filters.length; i++) {
				tagFilter += tagFilter + stringToTag(filters[i].innerHTML) + " ";
			}
			filterEntries();
		});
		div.appendChild(button);
	}
}

function filterEntries() {
	let entries = document.getElementsByClassName("entry");
	for (let i = 0; i < entries.length; i++) {
		if (tagFilter == "") {
			entries[i].style.display = "block";
		} else {
			entries[i].style.display = "none";
		}
	}

	entries = document.getElementsByClassName(tagFilter);
	for (let i = 0; i < entries.length; i++) {
		entries[i].style.display = "block";
	}
}
