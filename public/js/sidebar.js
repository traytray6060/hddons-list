// Collapsable sidebar

function handleSidebar() {
	let sButton = document.getElementById("sbutton");
	let sidebar = document.getElementById("sidebar");

	if (sidebar.style.visibility == "visible") {
		sButton.innerHTML = ">";
		sButton.style.left = 0;
		sidebar.style.visibility = "hidden";
	} else {
		sButton.innerHTML = "<";
		sButton.style.left = "270px";
		sidebar.style.visibility = "visible";
	}
}
