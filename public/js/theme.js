// Theme selector

document.addEventListener("DOMContentLoaded", function(event) {
	var themeSelector = document.getElementById("themeSelector")

	themeSelector.addEventListener("change", function(event) {
		console.log(event.target.value);
		document.documentElement.classList = event.target.value;
		localStorage.setItem("hddons-list-theme", event.target.value);
	});

	var savedTheme = localStorage.getItem("hddons-list-theme");

	// If no previously selected theme, just use the default one
	if (!savedTheme) {
		localStorage.setItem("hddons-list-theme", "nord");
	} else {
		themeSelector.value = savedTheme;
	}
	document.documentElement.classList = themeSelector.value;
});
