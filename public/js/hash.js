//
//     ####       ####
//     ####       ####
// #######################
// #######################
//     ####       ####
//     ####       ####
//     ####       ####
//     ####       ####
// #######################
// #######################
//     ####       ####
//     ####       ####
//

window.onhashchange = function() {
	let hash = location.hash.substr(1);

	// check if category exists
	let temp = hash.split(":");
	let button = document.getElementById("#" + temp[0]);
	if (button != null) {
		selectButton(button);
		processHash(hash);
	}
};

function processHash(hash) {
	let hi = hash.split(":");

	if (hi.length >= 2) {
		buildEntries(hi[0], hi[1])
	} else {
		buildEntries(hi[0], null)
	}
}
