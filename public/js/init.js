document.addEventListener("DOMContentLoaded", function(event) {
	let hash = location.hash.substr(1);

	// activate button
	let temp = hash.split(":");
	let button = document.getElementById("#" + temp[0]);

	if (button != null) {
		button.classList.add("selected");
		processHash(hash);
	} else {
		buildEntries("about", null);
	}
});
